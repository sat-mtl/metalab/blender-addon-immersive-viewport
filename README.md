### About
This repository adds immersive rendering to Blender viewports, through spherical, equirectangular or cubemap rendering.
Note that it has been developer and tested on Linux, using it on any other platform may require some additional work.

### License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version (http://www.gnu.org/licenses/).

### Authors
* François Ubald Brien
* Emmanuel Durand
* Jérémie Soria

### Projet URL
This project can be found on the [SAT Metalab repository](https://gitlab.com/sat-metalab/blender-addon-immersive-viewport.git)

# Requirements
To install the requirements, run the following command:
```bash
sudo pip3 install pyopengl pyopengl-accelerate
```

Also note that this addon has been developed and tested on Linux. Using it on any other platform may require some additional work.

### Installation
This addon can be installed in two ways:
* if you are a developer, you can install a symbolic link to your Blender addon directory with the _install.sh_ script.
* if you are a user, you can "build" the addon with the _build.sh_ script, then install the resulting _blender-immersive-viewport.zip_ file like any other Blender addon

### Sponsors
This project is made possible thanks to the [Society for Arts and Technologies](http://www.sat.qc.ca) (also known as SAT).
Thanks to the Ministère de l'Économie, de la Science et de l'Innovation du Québec (MESI).
