#!/bin/bash
cd immersiveViewport
version=`blender --version | egrep -o "Blender [0-9.]+" | egrep -o "[0-9.]+"`
mkdir -p ~/.config/blender/$version/scripts/addons/immersiveViewport
cp -v * ~/.config/blender/$version/scripts/addons/immersiveViewport/
