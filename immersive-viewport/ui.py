from bpy.types import Panel

import imp
if "operators" in locals():
    imp.reload(operators)


class RenderPanel(Panel):
    bl_idname = "RENDER_PT_immersive_viewport"
    bl_label = "Immersive Viewport"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "render"
    bl_options = {'DEFAULT_CLOSED'}

    def draw(self, context):
        preferences = context.user_preferences.addons[__package__].preferences
        layout = self.layout

        col = layout.column(align=True)
        row = col.row(align=True)
        row.prop(preferences, "subrenderResolution", text="Cubemap resolution")

        row = col.row(align=True)
        row.separator()
        row = col.row(align=True)
        row.label("Projection parameters:")
        row = col.row(align=True)
        row.prop(preferences, "anchorObject")
        row = col.row(align=True)
        row.prop(preferences, "projectionType", text="")

        if preferences.projectionType == "Spherical":
            row.prop(preferences, "fieldOfView", text="FOV")
            row = col.row(align=True)
            row.separator()
            row = col.row(align=True)
            row.label("Dome parameters")
            row = col.row(align=True)
            row.prop(preferences, "shiftFromCenter", text="")
            row = col.row(align=True)
            row.prop(preferences, "domeRadius", text="Dome radius")


class ImmersiveViewportToolbar:
    bl_label = "Immersion"
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'

    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'

    def draw(self, context):
        layout = self.layout

        col = layout.column(align=True)
        row = col.row(align=True)
        row.scale_y = 2
        row.operator("immersive.toggle", icon='WIRE')


class ImmersiveViewport(Panel, ImmersiveViewportToolbar):
    bl_category = "Live Editing"
