import bpy
import OpenGL

from bgl import *
from mathutils import *
from math import pi, tan

class GLView():
    def __init__(self, context, width, height, anchor, rotationMatrix):
        import gpu
        scene = context.scene

        try:
            self._offscreen = gpu.offscreen.new(width, height)
        except Exception as e:
            print(e)
            self._offscreen = None

        if bpy.data.objects.find(anchor) != -1:
            self._anchor = bpy.data.objects[anchor]
        else:
            self._anchor = scene.camera

        self._width = width
        self._height = height
        self._rotationMatrix = rotationMatrix

        self._fov = pi / 2.0
        self._near = 0.1
        self._far = 1000.0
        self._cx = 0.5
        self._cy = 0.5

        self._shiftX = 0.0
        self._shiftY = 0.0
        self._shiftZ = 0.0

    def __del__(self):
        if self._offscreen is not None:
            self._offscreen.free()

    def getTexture(self):
        return self._offscreen.color_texture

    def draw(self, context):
        scene = context.scene
        aspect_ratio = scene.render.resolution_x / scene.render.resolution_y
        self.updateOffscreen(context, self._offscreen)

    def setFrustum(self, fov, near, far, shiftX, shiftY):
        self._fov = fov
        self._near = near
        self._far = far
        self._cx = shiftX
        self._cy = shiftY

    def shiftPosition(self, x, y, z):
        self._shiftX = x;
        self._shiftY = y;
        self._shiftZ = z;

    def computeProjectionMatrix(self):
        near = self._near
        far = self._far
        
        topTemp = near * tan(self._fov / 2.0)
        bottomTemp = -topTemp
        top = topTemp - (self._cy - 0.5) * (topTemp - bottomTemp)
        bottom = bottomTemp - (self._cy - 0.5) * (topTemp - bottomTemp)

        rightTemp = topTemp * float(self._width) / float(self._height)
        leftTemp = bottomTemp * float(self._width) / float(self._height)
        right = rightTemp - (self._cx - 0.5) * (rightTemp - leftTemp)
        left = leftTemp - (self._cx - 0.5) * (rightTemp - leftTemp)

        mat = Matrix.Identity(4)
        mat[0][0] = 2.0 * near / (right - left)
        mat[0][2] = (right + left) / (right - left)
        mat[1][1] = 2.0 * near / (top - bottom)
        mat[1][2] = (top + bottom) / (top - bottom)
        mat[2][2] = - (far + near) / (far - near)
        mat[2][3] = -2.0 * far * near / (far - near)
        mat[3][2] = -1.0
        mat[3][3] = 0.0
        return mat

    def updateOffscreen(self, context, offscreen):
        scene = context.scene
        render = scene.render
        anchor = self._anchor

        modelViewMatrix = self._rotationMatrix * anchor.matrix_world.inverted_safe()
        
        # Move the anchor center
        rotationMatrix = modelViewMatrix.to_quaternion()
        rotationMatrix = rotationMatrix.to_matrix().to_4x4()
        shiftVec = Vector((self._shiftX, self._shiftY, self._shiftZ))
        shiftVec = rotationMatrix * shiftVec
        modelViewMatrix.translation += shiftVec

        ## Get the projection matrix according to the frustum parameters
        projectionMatrix = self.computeProjectionMatrix()

        offscreen.draw_view3d(
            scene,
            context.space_data, 
            context.region,
            projectionMatrix,
            modelViewMatrix
            )
